package code;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class SummativeIntermediateTest {

    private final PrintStream originalSystemOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @AfterEach
    public void tearDown() {
        System.setOut(originalSystemOut);
    }

    //possitive test

    @Test
    void testMainCorrectOutput() {
        // Arrange
        String[] args = { "1", "2", "3" };

        // Act
        SummativeIntermediate.Main(args);

        // Assert
        String expectedOutput = "1\n2\n3\n";
        String cleanedExpectedOutput = cleanString(expectedOutput);
        String cleanedActualOutput = cleanString(outputStreamCaptor.toString());

        assertEquals(cleanedExpectedOutput, cleanedActualOutput);
    }

    @Test
    void testMainNotThrowExceptionValidArguments() {
        // Arrange
        String[] args = { "1", "2", "3" };

        // Act & Assert
        assertDoesNotThrow(() -> SummativeIntermediate.Main(args));
    }

    //negative test
    @Test
    void testMainWithEmptyArguments() {
        // Arrange
        String[] args = {};

        // Act
        SummativeIntermediate.Main(args);

        // Assert
        assertEquals("", outputStreamCaptor.toString().trim());
    }

    @Test
    void testMainThrowsExceptionWithInvalidArguments() {
        // Arrange
        String[] args = { "1", "two", "3" };

        // Act & Assert
        assertThrows(NumberFormatException.class, () -> SummativeIntermediate.Main(args));
    }

    private String cleanString(String input) {
        return input.trim().replace("\r", "");
    }
  
    @Test
    void testMainNotThrowExceptionWithEmptyArguments() {
        // Arrange
        String[] args = {};

        // Act & Assert
        assertDoesNotThrow(() -> SummativeIntermediate.Main(args));
    }

    @Test
    void testMainDoesNotPrintAnythingWithEmptyArguments() {
        // Arrange
        String[] args = {};

        // Mock System.out
        PrintStream mockedPrintStream = mock(PrintStream.class);
        System.setOut(mockedPrintStream);

        // Act
        SummativeIntermediate.Main(args);

        // Verify that System.out.println is not called
        verify(mockedPrintStream, never()).println(anyString());
    }
}
