public class MainApp {
    public static void main(String[] args) {
        String fileName = "traffic-light1.txt";
        Intersection intersection = new Intersection();
        int durationInSeconds = 300; 

        // Mulai simulasi
        intersection.simulate(durationInSeconds, fileName);

        System.out.println("Simulasi selesai. Hasil disimpan dalam file: " + fileName);
    }
}
