public class Intersection {
    private TrafficLight[] trafficLights;
    private int currentGreenLightIndex;

    public Intersection() {
        trafficLights = new TrafficLight[4];
        for (int i = 0; i < 4; i++) {
            trafficLights[i] = new TrafficLight(i + 1);
        }
        currentGreenLightIndex = 0;
    }

    public void simulate(int durationInSeconds, String fileName) {
        for (int i = 0; i < durationInSeconds; i++) {
            for (TrafficLight light : trafficLights) {
                light.tick(fileName);
            }
            updateGreenLight();
            try {
                Thread.sleep(1000); // Pause for 1 second
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateGreenLight() {
        trafficLights[currentGreenLightIndex].tick(null);
        currentGreenLightIndex = (currentGreenLightIndex + 1) % 4;
        trafficLights[currentGreenLightIndex].tick(null);
    }
}
