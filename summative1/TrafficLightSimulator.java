// import java.io.BufferedWriter;
// import java.io.File;
// import java.io.FileWriter;
// import java.io.IOException;
// import java.time.LocalTime;

// public class TrafficLightSimulator {

//     private static final int DURATION_GREEN = 5; // Durasi lampu hijau (detik)
//     private static final int DURATION_ORANGE = 1; // Durasi lampu oranye (detik)
//     private static final int DURATION_CROSSWALK = 2; // Durasi hak penyebrang jalan (detik)

//     public static void main(String[] args) {
//         String folderPath = "summative1";
//         File folder = new File(folderPath);
//         try (BufferedWriter writer = new BufferedWriter(new FileWriter(folder + "/traffic-light1.txt"))) {
//             int currentLightIndex = 0;

//             // Simulasi selama 30 menit
//             for (int i = 0; i < 1 * 60; i++) { //ganti dulu dengan 1 menit untuk percobaan
//                 TrafficLight[] lights = new TrafficLight[4];

//                 // Inisialisasi lampu
//                 for (int j = 0; j < 4; j++) {
//                     lights[j] = new TrafficLight();
//                     lights[j].setId(j + 1);
//                 }

//                 // Hitung waktu saat ini
//                 LocalTime currentTime = LocalTime.of(12, 0).plusSeconds(i);

//                 // Hitung indeks lampu yang harus menjadi hijau berdasarkan waktu saat ini
//                 currentLightIndex = (currentTime.getMinute() / 2) % 4;

//                 // Atur status lampu
//                 for (int j = 0; j < 4; j++) {
//                     if (j == currentLightIndex) {
//                         lights[j].setGreen(true);
//                     } else {
//                         lights[j].setGreen(false);
//                     }
//                 }

//                 // Tulis hasil simulasi ke file
//                 writer.write("Time: " + currentTime + "\n");
//                 for (int j = 0; j < 4; j++) {
//                     writer.write(lights[j].toString() + "\n");
//                 }
//                 writer.write("\n");

//                 // Tunggu 1 detik (dapat diubah sesuai kebutuhan)
//                 Thread.sleep(1000);
//             }
//         } catch (IOException | InterruptedException e) {
//             e.printStackTrace();
//         }
//     }
// }


