package summative2;

public class ParkingRecord {
    private Vehicle vehicle;
    private int minutes;

    public ParkingRecord(Vehicle vehicle, int minutes) {
        this.vehicle = vehicle;
        this.minutes = minutes;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public int getMinutes() {
        return minutes;
    }
}
