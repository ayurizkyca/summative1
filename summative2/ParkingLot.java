package summative2;

import java.util.ArrayList;
import java.util.List;

class ParkingLot {
    private List<ParkingRecord> parkingRecords;

    public ParkingLot() {
        this.parkingRecords = new ArrayList<>();
    }

    public void parkVehicle(Vehicle vehicle, int minutes) {
        parkingRecords.add(new ParkingRecord(vehicle, minutes));
    }

    public void displayPaymentInfo() {
        System.out.println("Payment Information:");

        for (ParkingRecord record : parkingRecords) {
            int parkingFee = record.getVehicle().calculateParkingFee(record.getMinutes());

            System.out.println("License Plate: " + record.getVehicle().getLicensePlate());
            System.out.println("Park Duration: " + record.getMinutes() + " minutes");
            System.out.println("Parking Fee: Rp. " + parkingFee);
            System.out.println("--------------------");
        }
    }
}
