package summative2;

public class ParkingApp {

    public static void main(String[] args) {
        // Membuat objek tempat parkir
        ParkingLot parkingLot = new ParkingLot();

        // Membuat beberapa objek kendaraan dan memarkirnya
        //mobil
        Vehicle car1 = new Car("AB 1234 CD");
        Vehicle car2 = new Car("EF 5678 GH");
        Vehicle car3 = new Car("GH 1234 AB");
        Vehicle car4 = new Car("CD 5678 GH");
        Vehicle car5 = new Car("HI 7891 EF");

        //motor
         //mobil
         Vehicle motorcycle1 = new Motorcycle("AB 1234 MN");
         Vehicle motorcycle2 = new Motorcycle("EF 5678 MN");
         Vehicle motorcycle3 = new Motorcycle("GH 1234 MN");
         Vehicle motorcycle4 = new Motorcycle("CD 5678 MN");
         Vehicle motorcycle5 = new Motorcycle("HI 7891 MN");

        //simulasi parkir mengunakan motor
        parkingLot.parkVehicle(motorcycle1, 30); // Parkir selama 30 menit
        parkingLot.parkVehicle(motorcycle2, 62); // Parkir selama 1 jam dan 2 menit
        parkingLot.parkVehicle(motorcycle3, 240); // Parkir selama 4 jam
        parkingLot.parkVehicle(motorcycle4, 419); // Parkir selama 6 jam dan 59 menit
        parkingLot.parkVehicle(motorcycle5, 2400); // Parkir selama 40 jam

        //simulasi parkir menggunakan mobil
        parkingLot.parkVehicle(car1, 30); // Parkir selama 30 menit
        parkingLot.parkVehicle(car2, 62); // Parkir selama 1 jam dan 2 menit
        parkingLot.parkVehicle(car3, 240); // Parkir selama 4 jam
        parkingLot.parkVehicle(car4, 419); // Parkir selama 6 jam dan 59 menit
        parkingLot.parkVehicle(car5, 2400); // Parkir selama 40 jam

        // Menampilkan informasi pembayaran
        parkingLot.displayPaymentInfo();
    }
}







