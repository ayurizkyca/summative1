package summative2;

class Car extends Vehicle {
    public Car(String licensePlate) {
        super(licensePlate);
    }

    @Override
    public int calculateParkingFee(int minutes) {
        int hours = (int) Math.ceil((double) minutes / 60);

        if (hours == 0) {
            return 0; // Gratis jika parkir kurang dari 1 jam
        } else if (hours == 1) {
            return 5000;
        } else {
            return 5000 + 4500 * (hours - 1);
        }
    }
}
