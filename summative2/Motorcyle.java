package summative2;

class Motorcycle extends Vehicle {
    public Motorcycle(String licensePlate) {
        super(licensePlate);
    }

    @Override
    public int calculateParkingFee(int minutes) {
        int hours = (int) Math.ceil((double) minutes / 60);

        if (hours == 0) {
            return 0; // Gratis jika parkir kurang dari 1 jam
        } else if (hours == 1) {
            return 4000;
        } else {
            return 4000 + 3500 * (hours - 1);
        }
    }
}
