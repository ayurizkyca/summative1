package summative4;

public class LiftSimulation {
    public static void main(String[] args) {
        // Inisialisasi lift di lantai 3
        Lift lift = new Lift(3);

        // Calon penumpang dari lantai parkir ke lantai GF dan 3
        Passenger passenger1 = new Passenger(0, 0); // Lantai Parkir ke GF
        Passenger passenger2 = new Passenger(0, 3); // Lantai Parkir ke Lantai 3

        // Penumpang dari lantai 1 ke lantai 3
        Passenger passenger3 = new Passenger(1, 3); // Lantai 1 ke Lantai 3

        // Simulasi penumpang masuk ke lift
        lift.addPassenger(passenger1);
        lift.addPassenger(passenger2);
        lift.addPassenger(passenger3);

        // Simulasi lift bergerak
        lift.move();
        lift.move();
        lift.move();
    }
}

