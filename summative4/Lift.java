package summative4;

import java.util.LinkedList;
import java.util.Queue;

public class Lift {
    private int currentFloor;
    private Direction direction;
    private Queue<Passenger> passengers;

    public Lift(int initialFloor) {
        this.currentFloor = initialFloor;
        this.direction = Direction.NONE;
        this.passengers = new LinkedList<>();
    }

    public void move() {
        if (!passengers.isEmpty()) {
            Passenger passenger = passengers.poll();
            int destinationFloor = passenger.getDestinationFloor();
            moveTowards(destinationFloor);
        } else if (currentFloor != 0) {
            moveTowards(0); // Move towards floor 0 if there are no passengers
        }
    }

    public void addPassenger(Passenger passenger) {
        passengers.add(passenger);
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public Direction getDirection() {
        return direction;
    }

    public boolean hasPassengerForCurrentFloor() {
        for (Passenger passenger : passengers) {
            if (passenger.getDestinationFloor() == currentFloor) {
                return true;
            }
        }
        return false;
    }

    private void moveTowards(int destinationFloor) {
        if (currentFloor < destinationFloor) {
            direction = Direction.UP;
            while (currentFloor < destinationFloor) {
                currentFloor++;
                System.out.println("Lift is moving UP to floor " + currentFloor);
                if (hasPassengerForCurrentFloor()) {
                    unloadPassengers();
                }
            }
        } else if (currentFloor > destinationFloor) {
            direction = Direction.DOWN;
            while (currentFloor > destinationFloor) {
                currentFloor--;
                System.out.println("Lift is moving DOWN to floor " + currentFloor);
                if (hasPassengerForCurrentFloor()) {
                    unloadPassengers();
                }
            }
        }
        direction = Direction.NONE;
        System.out.println("Lift has reached destination floor " + currentFloor);
        unloadPassengers();
    }

    private void unloadPassengers() {
        passengers.removeIf(passenger -> passenger.getDestinationFloor() == currentFloor);
        System.out.println("Passengers have exited the lift at floor " + currentFloor);
    }
}

enum Direction {
    UP, DOWN, NONE
}
