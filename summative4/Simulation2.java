package summative4;

public class Simulation2 {
    //posisi lift lt.2
    //calon penumpang di lt.3 ingin ke 0
    //calon penumbang di lt.1 ingin ke lt3
    //calon penumpang di 0 ingin ke lt 3

    public static void main(String[] args) {
         // Inisialisasi lift di lantai 2
         Lift lift = new Lift(2);

         // Calon penumpang dari lantai 1 ke lantai GF
         Passenger passenger1 = new Passenger(0, 3);
         // Calon penumpang dari lantai 3 ke lantai 0
         Passenger passenger2 = new Passenger(1, 3); // Lantai Parkir ke Lantai 3
 
         // Simulasi penumpang masuk ke lift
         lift.addPassenger(passenger1);
         lift.addPassenger(passenger2);
 
         // Simulasi lift bergerak
         lift.move();
         lift.move();
    }
}
