package summative4;

public class Simulation1 {
    public static void main(String[] args) {
        // posisi lift lt.0
        // 1 penumpang lt.1, 1 penumpang lt.3 dan ingin ke lt.0

        // Inisialisasi lift di lantai 0
        Lift lift = new Lift(0);

        // Calon penumpang dari lantai 1 ke lantai GF
        Passenger passenger1 = new Passenger(1, 0);
        // Calon penumpang dari lantai 3 ke lantai 0
        Passenger passenger2 = new Passenger(3, 0); // Lantai Parkir ke Lantai 3

        // Simulasi penumpang masuk ke lift
        lift.addPassenger(passenger1);
        lift.addPassenger(passenger2);

        // Simulasi lift bergerak
        lift.move();
        lift.move();
    }
}
