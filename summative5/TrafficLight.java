import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class TrafficLight {
    private int id;
    private LightColor color;
    private int timer;

    public TrafficLight(int id) {
        this.id = id;
        this.color = LightColor.RED;
        this.timer = 0;
    }

    public void tick(String fileName) {
        timer++;
        if (timer == 120) {
            switchColor();
            timer = 0;
            saveScenario(fileName);
        } else if (timer == 115 && color == LightColor.GREEN) {
            switchColor(); // 5 detik sebelum hijau berubah menjadi merah
        }
    }

    private void saveScenario(String fileName) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) {
            writer.write("Traffic Light " + id + ": " + color + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void switchColor() {
        if (color == LightColor.RED) {
            color = LightColor.GREEN;
        } else if (color == LightColor.GREEN) {
            color = LightColor.ORANGE;
        } else {
            color = LightColor.RED;
        }
    }

    @Override
    public String toString() {
        return "Traffic Light " + id + ": " + color;
    }
}
