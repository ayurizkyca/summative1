import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class TrafficSimulation {

    private static final int NUMBER_OF_LANES = 4;
    private static final int MAX_TRAFFIC_DENSITY = 200;
    private static int GREEN_LIGHT_DURATION = 120;
    private static final int ADDITIONAL_TIME_FOR_TRAFFIC_JAM = 30;

    public void simulateTraffic(String fileName, int durationInSeconds) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            Random random = new Random();

            for (int i = 0; i < durationInSeconds; i++) {
                int[] trafficDensity = generateTrafficDensity(random);
                boolean isTrafficJammed = checkTrafficJam(trafficDensity);

                writer.write("Simulation Iteration: " + (i + 1) + "\n");
                for (int j = 0; j < NUMBER_OF_LANES; j++) {
                    writer.write("Lane " + (j + 1) + ": " + trafficDensity[j] + " vehicles");
                    if (isTrafficJammed && j == findMostCongestedLane(trafficDensity)) {
                        writer.write(" (Traffic Jammed)");
                    }
                    writer.write("\n");
                }
                writer.write("\n");

                if (isTrafficJammed) {
                    GREEN_LIGHT_DURATION += ADDITIONAL_TIME_FOR_TRAFFIC_JAM;
                }

                // Simulasi lalu lintas
                simulateTrafficLights(fileName);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int[] generateTrafficDensity(Random random) {
        int[] trafficDensity = new int[NUMBER_OF_LANES];
        for (int i = 0; i < NUMBER_OF_LANES; i++) {
            trafficDensity[i] = random.nextInt(MAX_TRAFFIC_DENSITY + 1);
        }
        return trafficDensity;
    }

    private boolean checkTrafficJam(int[] trafficDensity) {
        int mostCongestedLane = findMostCongestedLane(trafficDensity);
        int secondMostCongestedLane = findSecondMostCongestedLane(trafficDensity);

        return trafficDensity[mostCongestedLane] > 1.3 * trafficDensity[secondMostCongestedLane];
    }

    private int findMostCongestedLane(int[] trafficDensity) {
        int mostCongestedLane = 0;
        for (int i = 1; i < trafficDensity.length; i++) {
            if (trafficDensity[i] > trafficDensity[mostCongestedLane]) {
                mostCongestedLane = i;
            }
        }
        return mostCongestedLane;
    }

    private int findSecondMostCongestedLane(int[] trafficDensity) {
        int mostCongestedLane = findMostCongestedLane(trafficDensity);
        int secondMostCongestedLane = (mostCongestedLane + 1) % trafficDensity.length;

        for (int i = 0; i < trafficDensity.length; i++) {
            if (i != mostCongestedLane && trafficDensity[i] > trafficDensity[secondMostCongestedLane]) {
                secondMostCongestedLane = i;
            }
        }
        return secondMostCongestedLane;
    }

    private void simulateTrafficLights(String fileName) {
        Intersection intersection = new Intersection();
        intersection.simulate(GREEN_LIGHT_DURATION, fileName);
    }
}

