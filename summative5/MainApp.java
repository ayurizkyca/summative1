public class MainApp {
    public static void main(String[] args) {
        String trafficSimulationFileName = "smart-traffic-light2.txt";
        TrafficSimulation trafficSimulation = new TrafficSimulation();
        int durationInSeconds = 300;

        trafficSimulation.simulateTraffic(trafficSimulationFileName, durationInSeconds);

        System.out.println("Simulasi lalu lintas selesai. Hasil disimpan dalam file: " + trafficSimulationFileName);
    }
}
